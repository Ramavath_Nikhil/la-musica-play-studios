package com.visitome.olaplaystudios.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.visitome.olaplaystudios.Models.PlayList;
import com.visitome.olaplaystudios.Models.Song;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikhilramavath on 17/12/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "contactsManager";

    // Contacts table name
    private static final String TABLE_SONGS = "songs";
    private static final String TABLE_PLAYLISTS = "playlists";
    private static final String TABLE_SONG_PLAYLISTS = "songsplaylists";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_SONG = "song";
    private static final String KEY_URL = "url";
    private static final String KEY_ARTIST = "artist";
    private static final String KEY_COVER_IMAGE = "cover_image";
    private static final String KEY_DOWNLOADED_LOCAL_PATH = "downloaded_local_path";
    private static final String KEY_IS_FAVOURITE = "key_is_favourite";
    private static final String KEY_PLAYLIST_NAME = "key_playlist_name";

    private static DatabaseHandler sInstance;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public static synchronized DatabaseHandler getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new DatabaseHandler(context.getApplicationContext());
        }
        return sInstance;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_SONGS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_SONG + " TEXT,"
                + KEY_URL + " TEXT," + KEY_ARTIST + " TEXT,"
                + KEY_COVER_IMAGE + " TEXT," + KEY_DOWNLOADED_LOCAL_PATH + " TEXT," + KEY_IS_FAVOURITE + " INTEGER DEFAULT 0" + ")";

//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SONGS);
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed


        // Create tables again
        onCreate(db);
    }

    public void addSong(Song song) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SONG, song.getSong());
        values.put(KEY_URL, song.getUrl());
        values.put(KEY_ARTIST, song.getArtists());
        values.put(KEY_COVER_IMAGE, song.getCoverImage());

        // Inserting Row
        db.insert(TABLE_SONGS, null, values);
        db.close(); // Closing database connection
    }

    public void addSongs(List<Song> songs) {
        SQLiteDatabase db = this.getWritableDatabase();

//        db.execSQL("DELETE FROM " + TABLE_SONGS);
        for (Song song : songs) {

            if (!isRecordExistInDatabase(song.getUrl())) {
                Log.d("adding song", song.getSong());
                ContentValues values = new ContentValues();
                values.put(KEY_SONG, song.getSong());
                values.put(KEY_URL, song.getUrl());
                values.put(KEY_ARTIST, song.getArtists());
                values.put(KEY_COVER_IMAGE, song.getCoverImage());

                // Inserting Row
                db.insert(TABLE_SONGS, null, values);
            } else {
                Log.d("song not added", "" + song.getSong());
            }
        }

        db.close(); // Closing database connection
    }

    public int updateDownloadedLocalPath(String path, String url) {


        Log.d("downlaod path", path + " " + url);
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DOWNLOADED_LOCAL_PATH, path);
        return db.update(TABLE_SONGS, values, KEY_URL + " = ?",
                new String[]{String.valueOf(url)});

    }

    public int updateFavouriteSongs(int isFavourite, String url) {


        Log.d("downlaod path", isFavourite + " " + url);
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IS_FAVOURITE, isFavourite);
        return db.update(TABLE_SONGS, values, KEY_URL + " = ?",
                new String[]{String.valueOf(url)});

    }

    public List<Song> getDownloadedSongs() {

        Cursor cursor = null;
        ArrayList<Song> songList = new ArrayList<Song>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SONGS;

        SQLiteDatabase db = this.getWritableDatabase();
        cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Song song = new Song();

                if (cursor.getString(5) != null) {
                    song.setSong(cursor.getString(1));
                    song.setUrl(cursor.getString(2));
                    song.setArtists(cursor.getString(3));
                    song.setCoverImage(cursor.getString(4));
                    song.setDownloadedPath(cursor.getString(5));

                    if (cursor.getInt(6) == 0)
                        song.setFavourite(false);
                    else
                        song.setFavourite(true);

                    // Adding contact to list
                    songList.add(song);
                }
            } while (cursor.moveToNext());
        }

        // return contact list
        return songList;


    }

    public List<Song> getFavouriteSongs() {

        Cursor cursor = null;
        ArrayList<Song> songList = new ArrayList<Song>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SONGS;

        SQLiteDatabase db = this.getWritableDatabase();
        cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Song song = new Song();

                if (cursor.getInt(6) != 0) {
                    song.setSong(cursor.getString(1));
                    song.setUrl(cursor.getString(2));
                    song.setArtists(cursor.getString(3));
                    song.setCoverImage(cursor.getString(4));
                    song.setDownloadedPath(cursor.getString(5));

                    if (cursor.getInt(6) == 0)
                        song.setFavourite(false);
                    else
                        song.setFavourite(true);

                    // Adding contact to list
                    songList.add(song);
                }
            } while (cursor.moveToNext());
        }

        // return contact list
        return songList;


    }


    private boolean isRecordExistInDatabase(String value) {

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_SONGS + " WHERE " + KEY_URL + " = '" + value + "'";
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            //Record exist
            c.close();
            return true;
        }
        //Record available
        c.close();
        return false;
    }


    // Getting single song
    Song getSong(String url) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_SONGS + " WHERE " + KEY_URL + " = '" + url + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        ;
        if (cursor != null)
            cursor.moveToFirst();


        Song song = new Song();
        song.setSong(cursor.getString(1));
        song.setUrl(cursor.getString(2));
        song.setArtists(cursor.getString(3));
        song.setCoverImage(cursor.getString(4));
        song.setDownloadedPath(cursor.getString(5));
        if (cursor.getInt(6) == 0)
            song.setFavourite(false);
        else
            song.setFavourite(true);


        return song;
    }

    // Getting All Songs
    public ArrayList<Song> getAllSongs(int currentPage) {


        Cursor cursor = null;
        ArrayList<Song> songList = new ArrayList<Song>();
        if (currentPage == -1) {

            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_SONGS;

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

        } else {
            int lineSize = 5;
            String selectQuery = "SELECT  * FROM " + TABLE_SONGS + " LIMIT ?,? ";

            String selectionArgs[] = new String[]{
                    String.valueOf((currentPage - 1) * lineSize),
                    String.valueOf(lineSize)};

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, selectionArgs);
        }

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Song song = new Song();
                song.setSong(cursor.getString(1));
                song.setUrl(cursor.getString(2));
                song.setArtists(cursor.getString(3));
                song.setCoverImage(cursor.getString(4));
                song.setDownloadedPath(cursor.getString(5));
                if (cursor.getInt(6) == 0)
                    song.setFavourite(false);
                else
                    song.setFavourite(true);

                // Adding contact to list
                songList.add(song);
            } while (cursor.moveToNext());
        }

        // return contact list
        return songList;
    }

    public void createPlaylistTable() {
        SQLiteDatabase db = this.getReadableDatabase();
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_PLAYLISTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_PLAYLIST_NAME + " TEXT" + ")";

//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SONGS);
        db.execSQL(CREATE_CONTACTS_TABLE);
    }


    public void createSongPlaylistTable() {
        SQLiteDatabase db = this.getReadableDatabase();
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_SONG_PLAYLISTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_PLAYLIST_NAME + " TEXT," + KEY_URL + " TEXT" + ")";

//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SONGS);
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    public boolean addPlaylist(String playlistName) {


        if (!isPlaylistExistsInDatabase(playlistName)) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_PLAYLIST_NAME, playlistName);

            // Inserting Row
            db.insert(TABLE_PLAYLISTS, null, values);
            db.close(); // Closing database connection

            return true;
        }
        return false;
    }


    private boolean isPlaylistExistsInDatabase(String value) {

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_PLAYLISTS + " WHERE " + KEY_PLAYLIST_NAME + " = '" + value + "'";
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            //Record exist
            c.close();
            return true;
        }
        //Record available
        c.close();
        return false;
    }

    public boolean addSongToPlaylist(String songUrl, String playlistName) {

        if (!isSongExistsInPlaylistDatabase(songUrl, playlistName)) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_PLAYLIST_NAME, playlistName);
            values.put(KEY_URL, songUrl);

            // Inserting Row
            db.insert(TABLE_SONG_PLAYLISTS, null, values);
            db.close(); // Closing database connection

            return true;
        }
        return false;
    }

    private boolean isSongExistsInPlaylistDatabase(String songUrl, String playlistName) {

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_SONG_PLAYLISTS + " WHERE " + KEY_PLAYLIST_NAME + " = '" + playlistName + "'  AND " + KEY_URL + " = '" + songUrl + "'";
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            //Record exist
            c.close();
            return true;
        }
        //Record available
        c.close();
        return false;
    }

    public ArrayList<PlayList> getAllPlaylists() {
        ArrayList<PlayList> playLists = new ArrayList<>();
        try {
            Cursor cursor = null;

            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_PLAYLISTS;

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    PlayList playList = new PlayList();
                    playList.setId(cursor.getInt(0) + "");
                    playList.setName(cursor.getString(1));
                    playLists.add(playList);

                } while (cursor.moveToNext());
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
            createPlaylistTable();
            createSongPlaylistTable();
        }

        // return contact list
        return playLists;


    }


    public ArrayList<Song> getAllPlaylistSongs(String playListName) {
        ArrayList<Song> songs = new ArrayList<>();
        try {
            Cursor cursor = null;

            // Select All Query
            String query = "SELECT " + KEY_URL + " FROM " + TABLE_SONG_PLAYLISTS + " WHERE " + KEY_PLAYLIST_NAME + " = '" + playListName + "'";

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(query, null);


            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    String songUrl = cursor.getString(0);
                    songs.add(getSong(songUrl));
                } while (cursor.moveToNext());
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
            createPlaylistTable();
            createSongPlaylistTable();
        }

        // return contact list
        return songs;


    }


}
