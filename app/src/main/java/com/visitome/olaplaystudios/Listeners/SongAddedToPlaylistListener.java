package com.visitome.olaplaystudios.Listeners;

/**
 * Created by nikhilramavath on 20/12/17.
 */

public interface SongAddedToPlaylistListener {

    public abstract void onSuccess(boolean isAdded, String playlistName);
}
