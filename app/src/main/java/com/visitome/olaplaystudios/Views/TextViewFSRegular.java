package com.visitome.olaplaystudios.Views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class TextViewFSRegular extends TextView {
    public TextViewFSRegular(Context context) {
        super(context);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/FiraSans-Regular.ttf")); //fonts/quicksand_regular.ttf"));
    }

    public TextViewFSRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/FiraSans-Regular.ttf"));
    }

    public TextViewFSRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/FiraSans-Regular.ttf"));
    }

    @Override
    public boolean isInEditMode() {
        return true;
    }
}