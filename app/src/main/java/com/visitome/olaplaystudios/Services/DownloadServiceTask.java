package com.visitome.olaplaystudios.Services;


import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.visitome.olaplaystudios.Activites.MainActivity;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Sqlite.DatabaseHandler;
import com.visitome.olaplaystudios.Utils.Constants;
import com.visitome.olaplaystudios.Utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;
import static com.visitome.olaplaystudios.Services.MusicForegroundService.ACTION_MyUpdate;
import static com.visitome.olaplaystudios.Services.MusicForegroundService.EXTRA_KEY_UPDATE;

public class DownloadServiceTask extends Service {
    private static final String LOG_TAG = "ForegroundService";
    public static boolean IS_SERVICE_RUNNING = false;
    private String downloadUrl = "", downloadSongName = "";
    public static final String ACTION_DOWNLOADUpdate = "com.ola.play.Download";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(Constants.ACTION.DOWNLOAD_START)) {


            downloadUrl = intent.getStringExtra(Constants.ACTION.SONG_INDEX);
            downloadSongName = intent.getStringExtra(Constants.ACTION.DOWNLOAD_SONG_NAME);
            showNotification();
            new DownloadingTask().execute();


        } else if (intent.getAction().equals(
                Constants.ACTION.DOWNLOAD_END)) {
            Log.i(LOG_TAG, "Received Stop Foreground Intent");
            stopForeground(true);
            stopSelf();
        }
        return START_STICKY;
    }


    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);
            Log.d("download completed", "yes");
            stopForeground(true);
            stopSelf();


            Intent intentUpdate = new Intent();
            intentUpdate.setAction(ACTION_MyUpdate);
            intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);

            sendBroadcast(intentUpdate);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                Log.d("url from service", downloadUrl);
                downloadTask(downloadUrl);

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }

    private void downloadTask(String downloadUrl) {
        File apkStorage = null;
        File outputFile = null;
        String downloadFileName = downloadSongName + ".mp3";
        try {


            URL url = new URL(downloadUrl);//Create Download URl
            HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
            String redirect = c.getHeaderField("Location");
            if (redirect != null) {
                c = (HttpURLConnection) new URL(redirect).openConnection();
            }
            Log.d("Redirect url ", redirect);
            c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
            c.connect();//connect the URL Connection

            //If Connection response is not OK then show Logs
            if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                        + " " + c.getResponseMessage());

            }


            //Get File if SD card is present
            if (Utils.isSDCardPresent()) {

                apkStorage = new File(
                        Environment.getExternalStorageDirectory() + "/"
                                + Constants.downloadDirectory);
            }

            //If File is not present create directory
            if (!apkStorage.exists()) {
                apkStorage.mkdir();
                Log.e(TAG, "Directory Created.");
            }

            outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

            //Create New File if not present
            if (!outputFile.exists()) {
                outputFile.createNewFile();
                Log.e(TAG, "File Created");
            }


            FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

            InputStream is = c.getInputStream();//Get InputStream for connection

            byte[] buffer = new byte[1024];//Set buffer type
            int len1 = 0;//init length
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);//Write new file
            }
            Log.d("download response", DatabaseHandler.getInstance(getBaseContext()).updateDownloadedLocalPath(outputFile.getAbsolutePath(),downloadUrl)+"");


            //Close all connection after doing task
            fos.close();
            is.close();

        } catch (Exception e) {

            //Read exception if something went wrong
            e.printStackTrace();
            outputFile = null;
            Log.e(TAG, "Download Error Exception " + e.getMessage());
        }
    }

    private void showNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);


        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Ola Play studio")
                .setTicker("Ola Play Studio")
                .setContentText("Downloading " + downloadSongName)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true).build();

        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
                notification);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IS_SERVICE_RUNNING = false;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Used only in case if services are bound (Bound Services).
        return null;
    }
}