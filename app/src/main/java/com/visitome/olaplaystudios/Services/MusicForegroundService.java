package com.visitome.olaplaystudios.Services;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;

import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.visitome.olaplaystudios.Activites.MainActivity;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Utils.Constants;
import com.visitome.olaplaystudios.Utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by nikhilramavath on 17/12/17.
 */

public class MusicForegroundService extends Service implements ExoPlayer.EventListener {
    private static final String LOG_TAG = "MusicForegroundService";
    public static boolean IS_SERVICE_RUNNING = false;
    public static boolean IS_PLAYER_PAUSE = false;

    private SimpleExoPlayer player;

    private boolean playWhenReady = true;
    private int currentWindow;
    private long playbackPosition;
    private Handler handler;
    private static ArrayList<Song> currentPlayList = new ArrayList<>();


    public static final String ACTION_MyUpdate = "com.ola.play.UPDATE";
    public static final String EXTRA_KEY_UPDATE = "EXTRA_UPDATE";
    public static final String EXTRA_SONG_LENGTH = "SONG_LENGTH";
    public static final String EXTRA_SONG_CURRENT_TIME = "CURRENT_TIME";


    private static int selectedSongIndex = 0;

    @Override
    public void onCreate() {
        super.onCreate();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
            Log.i(LOG_TAG, "Received Start Foreground Intent ");
            handler = new Handler();

            selectedSongIndex = intent.getIntExtra(Constants.ACTION.SONG_INDEX, -1);
            showNotification(currentPlayList.get(selectedSongIndex).getSong(), currentPlayList.get(selectedSongIndex).getArtists());
            initializePlayer();

        } else if (intent.getAction().equals(Constants.ACTION.PREV_ACTION)) {

            if (player != null) {

                player.stop();
                selectedSongIndex--;
                if (selectedSongIndex < 0)
                    selectedSongIndex = currentPlayList.size() - 1;
                Uri uri = null;
                if (currentPlayList.get(selectedSongIndex).getDownloadedPath() != null && !currentPlayList.get(selectedSongIndex).getDownloadedPath().isEmpty()) {

                    uri = Uri.fromFile(new File(currentPlayList.get(selectedSongIndex).getDownloadedPath()));
                } else
                    uri = Uri.parse(currentPlayList.get(selectedSongIndex).getUrl());
                MediaSource mediaSource = buildMediaSource(uri);
                player.prepare(mediaSource, true, false);
                Utils.addSongToRecents(getBaseContext(), currentPlayList.get(selectedSongIndex).getUrl());
                updateNotification(currentPlayList.get(selectedSongIndex).getSong(), currentPlayList.get(selectedSongIndex).getArtists());


                Intent intentUpdate = new Intent();
                intentUpdate.setAction(ACTION_MyUpdate);
                intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
                intentUpdate.putExtra(Constants.ACTION.NEXT_ACTION, true);
                intentUpdate.putExtra(Constants.ACTION.SONG_INDEX, selectedSongIndex);

                sendBroadcast(intentUpdate);
            }


        } else if (intent.getAction().equals(Constants.ACTION.PLAY_PAUSE_ACTION)) {

            if (player.getPlayWhenReady()) {
                player.setPlayWhenReady(false);
            } else {
                player.setPlayWhenReady(true);
            }

        } else if (intent.getAction().equals(Constants.ACTION.NEXT_ACTION)) {

            nextSong();


        } else if (intent.getAction().equals(
                Constants.ACTION.STOPFOREGROUND_ACTION)) {
            Log.i(LOG_TAG, "Received Stop Foreground Intent");
            stopForeground(true);
            stopSelf();
        } else if (intent.getAction().equals(Constants.ACTION.CHANGE_SONG_ACTION)) {

            if (player == null) {
                handler = new Handler();

                selectedSongIndex = intent.getIntExtra(Constants.ACTION.SONG_INDEX, -1);
                updateNotification(currentPlayList.get(selectedSongIndex).getSong(), currentPlayList.get(selectedSongIndex).getArtists());
                initializePlayer();
            } else {
                player.stop();

                selectedSongIndex = intent.getIntExtra(Constants.ACTION.SONG_INDEX, -1);
                Uri uri = null;
                if (currentPlayList.get(selectedSongIndex).getDownloadedPath() != null && !currentPlayList.get(selectedSongIndex).getDownloadedPath().isEmpty()) {

                    uri = Uri.fromFile(new File(currentPlayList.get(selectedSongIndex).getDownloadedPath()));
                } else
                    uri = Uri.parse(currentPlayList.get(selectedSongIndex).getUrl());
                MediaSource mediaSource = buildMediaSource(uri);
                player.prepare(mediaSource, true, false);
                Utils.addSongToRecents(getBaseContext(), currentPlayList.get(selectedSongIndex).getUrl());
                updateNotification(currentPlayList.get(selectedSongIndex).getSong(), currentPlayList.get(selectedSongIndex).getArtists());
            }
        }


        return START_STICKY;
    }


    public static String getTimeString(long duration) {
        int minutes = (int) Math.floor(duration / 1000 / 60);
        int seconds = (int) ((duration / 1000) - (minutes * 60));
        return String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
    }

    public void nextSong() {
        if (player != null) {

            player.stop();

            if (Utils.getRepeatMode(getBaseContext()) == Constants.REPEAT_ONCE) {

                Uri uri = null;
                if (currentPlayList.get(selectedSongIndex).getDownloadedPath() != null && !currentPlayList.get(selectedSongIndex).getDownloadedPath().isEmpty()) {

                    uri = Uri.fromFile(new File(currentPlayList.get(selectedSongIndex).getDownloadedPath()));
                } else
                    uri = Uri.parse(currentPlayList.get(selectedSongIndex).getUrl());
                MediaSource mediaSource = buildMediaSource(uri);
                player.prepare(mediaSource, true, false);
                Utils.addSongToRecents(getBaseContext(), currentPlayList.get(selectedSongIndex).getUrl());
                updateNotification(currentPlayList.get(selectedSongIndex).getSong(), currentPlayList.get(selectedSongIndex).getArtists());


            } else {

                if (Utils.getShuffleMode(getBaseContext())) {

                    Random rand = new Random();
                    selectedSongIndex = rand.nextInt(currentPlayList.size());

                    if (selectedSongIndex > (currentPlayList.size() - 1))
                        selectedSongIndex = 0;

                    Uri uri = null;
                    if (currentPlayList.get(selectedSongIndex).getDownloadedPath() != null && !currentPlayList.get(selectedSongIndex).getDownloadedPath().isEmpty()) {

                        uri = Uri.fromFile(new File(currentPlayList.get(selectedSongIndex).getDownloadedPath()));
                    } else
                        uri = Uri.parse(currentPlayList.get(selectedSongIndex).getUrl());


                    MediaSource mediaSource = buildMediaSource(uri);
                    player.prepare(mediaSource, true, false);
                    Utils.addSongToRecents(getBaseContext(), currentPlayList.get(selectedSongIndex).getUrl());
                    updateNotification(currentPlayList.get(selectedSongIndex).getSong(), currentPlayList.get(selectedSongIndex).getArtists());


                } else {

                    if (selectedSongIndex == currentPlayList.size() - 1 && Utils.getRepeatMode(getBaseContext()) == Constants.NO_REPEAT) {

                    } else {
                        selectedSongIndex++;
                        if (selectedSongIndex > (currentPlayList.size() - 1))
                            selectedSongIndex = 0;

                        Uri uri = null;
                        if (currentPlayList.get(selectedSongIndex).getDownloadedPath() != null && !currentPlayList.get(selectedSongIndex).getDownloadedPath().isEmpty()) {

                            uri = Uri.fromFile(new File(currentPlayList.get(selectedSongIndex).getDownloadedPath()));
                        } else
                            uri = Uri.parse(currentPlayList.get(selectedSongIndex).getUrl());
                        MediaSource mediaSource = buildMediaSource(uri);
                        player.prepare(mediaSource, true, false);
                        Utils.addSongToRecents(getBaseContext(), currentPlayList.get(selectedSongIndex).getUrl());
                        updateNotification(currentPlayList.get(selectedSongIndex).getSong(), currentPlayList.get(selectedSongIndex).getArtists());
                    }

                }


            }


            Intent intentUpdate = new Intent();
            intentUpdate.setAction(ACTION_MyUpdate);
            intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
            intentUpdate.putExtra(Constants.ACTION.NEXT_ACTION, true);
            intentUpdate.putExtra(Constants.ACTION.SONG_INDEX, selectedSongIndex);
            sendBroadcast(intentUpdate);
        }
    }

    private void showNotification(String title, String message) {


        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
                getMyActivityNotification(title, message));

    }

    private void updateNotification(String title, String message) {


        Notification notification = getMyActivityNotification(title, message);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);

    }

    private Notification getMyActivityNotification(String title, String message) {

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Intent previousIntent = new Intent(this, MusicForegroundService.class);
        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
                previousIntent, 0);

        Intent playIntent = new Intent(this, MusicForegroundService.class);
        playIntent.setAction(Constants.ACTION.PLAY_PAUSE_ACTION);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                playIntent, 0);

        Intent nextIntent = new Intent(this, MusicForegroundService.class);
        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);


        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
                nextIntent, 0);


        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setTicker(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)

                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .addAction(android.R.drawable.ic_media_previous, "Previous",
                        ppreviousIntent)
                .addAction(android.R.drawable.ic_media_play, "Play",
                        pplayIntent)
                .addAction(android.R.drawable.ic_media_next, "Next",
                        pnextIntent).build();

        return notification;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        IS_SERVICE_RUNNING = false;
        releasePlayer();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Used only in case if services are bound (Bound Services).
        return null;
    }


    private void initializePlayer() {
        player = ExoPlayerFactory.newSimpleInstance(
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());

        player.setPlayWhenReady(playWhenReady);
        player.seekTo(currentWindow, playbackPosition);
        Uri uri = null;

        if (currentPlayList.get(selectedSongIndex).getDownloadedPath() != null && !currentPlayList.get(selectedSongIndex).getDownloadedPath().isEmpty()) {

            uri = Uri.fromFile(new File(currentPlayList.get(selectedSongIndex).getDownloadedPath()));
        } else
            uri = Uri.parse(currentPlayList.get(selectedSongIndex).getUrl());
        Utils.addSongToRecents(getBaseContext(), currentPlayList.get(selectedSongIndex).getUrl());
        MediaSource mediaSource = buildMediaSource(uri);
        player.prepare(mediaSource, true, false);
        player.addListener(this);


    }


    private MediaSource buildMediaSource(Uri uri) {


        if (currentPlayList.get(selectedSongIndex).getDownloadedPath() != null && !currentPlayList.get(selectedSongIndex).getDownloadedPath().isEmpty()) {


            return new ExtractorMediaSource(uri,
                    new DefaultDataSourceFactory(getBaseContext(), "ua"),
                    new DefaultExtractorsFactory(), null, null);

        } else {


            DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(
                    "ua",
                    null /* listener */,
                    DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                    DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,


                    true /* allowCrossProtocolRedirects */
            );

            return new ExtractorMediaSource(uri,
                    httpDataSourceFactory,
                    new DefaultExtractorsFactory(), null, null);
        }
    }

    public static void setCurrentPlayList(ArrayList<Song> currentPlayListSongs) {

        if (currentPlayList == null)
            currentPlayList = new ArrayList<>();

        currentPlayList.clear();
        currentPlayList.addAll(currentPlayListSongs);
    }

    public static void addMoreSongsToPlaylist(ArrayList<Song> moreSongs) {
        if (currentPlayList != null) {
            currentPlayList.addAll(moreSongs);
        } else {
            setCurrentPlayList(moreSongs);
        }

    }

    public static List<Song> getCurrentPlayList() {


        return currentPlayList;
    }

    public static String getCurrentPlayingSongUrl() {
        if (currentPlayList != null && currentPlayList.size() > 0)
            return currentPlayList.get(selectedSongIndex).getUrl();
        else
            return null;
    }

    public static void setCurrentPlayingPosition(int playingPosition) {


        selectedSongIndex = playingPosition;
    }

    public static int getCurrentPlayingPosition() {
        return selectedSongIndex;
    }


    private void releasePlayer() {
        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;

        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        if (player.getPlaybackState() == ExoPlayer.STATE_ENDED) {
            if (player != null) {

                player.stop();
                selectedSongIndex = selectedSongIndex + 1;
                if (selectedSongIndex > (currentPlayList.size() - 1))
                    selectedSongIndex = 0;

                Uri uri = null;
                if (currentPlayList.get(selectedSongIndex).getDownloadedPath() != null && !currentPlayList.get(selectedSongIndex).getDownloadedPath().isEmpty()) {

                    uri = Uri.fromFile(new File(currentPlayList.get(selectedSongIndex).getDownloadedPath()));
                } else
                    uri = Uri.parse(currentPlayList.get(selectedSongIndex).getUrl());

                MediaSource mediaSource = buildMediaSource(uri);
                player.prepare(mediaSource, true, false);

            }
        }


    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        updateProgressBar();

        if (playbackState == PlaybackState.STATE_PAUSED) {
            IS_PLAYER_PAUSE = true;
        } else if (playbackState == PlaybackState.STATE_PLAYING) {
            IS_PLAYER_PAUSE = false;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }


    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }


    private void updateProgressBar() {

        try {


            long duration = player == null ? 0 : player.getDuration();
            long position = player == null ? 0 : player.getCurrentPosition();

            // Remove scheduled updates.
            handler.removeCallbacks(updateProgressAction);

            int progress = (int) ((position * 100) / duration);
            //send update
            Intent intentUpdate = new Intent();
            intentUpdate.setAction(ACTION_MyUpdate);
            intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
            intentUpdate.putExtra(EXTRA_KEY_UPDATE, progress);
            intentUpdate.putExtra(EXTRA_SONG_LENGTH, getTimeString(duration));
            intentUpdate.putExtra(EXTRA_SONG_CURRENT_TIME, getTimeString(position));

            sendBroadcast(intentUpdate);


            if (progress >= 100) {
                nextSong();
            }

            // Schedule an update if necessary.
            int playbackState = player == null ? ExoPlayer.STATE_IDLE : player.getPlaybackState();
            if (playbackState != ExoPlayer.STATE_IDLE && playbackState != ExoPlayer.STATE_ENDED) {
                long delayMs;
                if (player.getPlayWhenReady() && playbackState == ExoPlayer.STATE_READY) {
                    delayMs = 1000 - (position % 1000);
                    if (delayMs < 200) {
                        delayMs += 1000;
                    }
                } else {
                    delayMs = 1000;
                }
                handler.postDelayed(updateProgressAction, delayMs);
            }
        } catch (Exception e) {
            handler.removeCallbacks(updateProgressAction);
        }
    }

    private final Runnable updateProgressAction = new Runnable() {
        @Override
        public void run() {
            updateProgressBar();
        }
    };
}

