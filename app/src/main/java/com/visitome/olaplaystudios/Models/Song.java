package com.visitome.olaplaystudios.Models;

/**
 * Created by nikhilramavath on 17/12/17.
 */


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Song implements Parcelable {

    @SerializedName("song")
    @Expose
    private String song;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("artists")
    @Expose
    private String artists;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;


    public String getDownloadedPath() {
        return downloadedPath;
    }

    public void setDownloadedPath(String downloadedPath) {
        this.downloadedPath = downloadedPath;
    }

    private String downloadedPath;
    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public static Creator<Song> getCREATOR() {
        return CREATOR;
    }

    private boolean isFavourite;

    public Song(Parcel in) {
        song = in.readString();
        url = in.readString();
        artists = in.readString();
        coverImage = in.readString();

    }

    public Song()
    {

    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(song);
        parcel.writeString(url);
        parcel.writeString(artists);
        parcel.writeString(coverImage);
    }
}