package com.visitome.olaplaystudios.Models;

/**
 * Created by nikhilramavath on 20/12/17.
 */

public class PlayList {

    private String id, name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
