package com.visitome.olaplaystudios.Adapters;

/**
 * Created by nikhilramavath on 17/12/17.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.visitome.olaplaystudios.Activites.MainActivity;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Services.MusicForegroundService;
import com.visitome.olaplaystudios.Sqlite.DatabaseHandler;
import com.visitome.olaplaystudios.Utils.Constants;
import com.visitome.olaplaystudios.Utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;
import static com.visitome.olaplaystudios.Utils.Utils.MyPREFERENCES;

public class SongsListAdapter extends RecyclerView.Adapter<SongsListAdapter.MyViewHolder> {

    private List<Song> songList;
    private Context mContext;
    private FloatingActionButton prevPlayedSongButton;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView iv_cover, iv_favourite, iv_share, iv_downloads;
        public TextView tv_title, tv_artist;
        public FloatingActionButton fabPlay;

        public MyViewHolder(View view) {
            super(view);

            iv_cover = view.findViewById(R.id.iv_cover);
            tv_title = view.findViewById(R.id.tv_title);
            tv_artist = view.findViewById(R.id.tv_artist);
            fabPlay = view.findViewById(R.id.fabPlay);
            iv_favourite = view.findViewById(R.id.iv_favourite);
            iv_share = view.findViewById(R.id.iv_share);
            iv_downloads = view.findViewById(R.id.iv_downlaods);
        }
    }


    public SongsListAdapter(List<Song> songList, Context context) {
        this.songList = songList;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_songs, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Song song = songList.get(position);


        holder.iv_cover.setImageBitmap(null);
        holder.tv_title.setText(song.getSong());
        holder.tv_artist.setText(song.getArtists());


        if (Utils.currentPlayingSongURL.equals(song.getUrl())) {


            if (((MainActivity) mContext).isPlayerPlaying()) {

                holder.fabPlay.setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));

            } else {

                holder.fabPlay.setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_play));
            }


        } else {
            holder.fabPlay.setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_play));
        }


        if (song.isFavourite()) {
            holder.iv_favourite.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_favorite));

        } else {

//            if (isFav(mContext, song.getUrl())) {
//                song.setFavourite(true);
//                holder.iv_favourite.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_favorite));
//            } else
                holder.iv_favourite.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_unfavorite));
        }

        if(song.getDownloadedPath()!=null && !song.getDownloadedPath().isEmpty())
        {
            holder.iv_downloads.setVisibility(View.GONE);
        }
        else
        {
            holder.iv_downloads.setVisibility(View.VISIBLE);
        }
        holder.fabPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (prevPlayedSongButton != null)
                    prevPlayedSongButton.setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_play));


                if (!Utils.currentPlayingSongURL.equals(song.getUrl())) {

                    MusicForegroundService.setCurrentPlayList((ArrayList<Song>) songList);
                    ((MainActivity) mContext).playSongFromAdapter(position);
                    prevPlayedSongButton = holder.fabPlay;
                    holder.fabPlay.setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));

                } else {
                    ((MainActivity) mContext).pausePlayer();

                }

            }
        });

        holder.iv_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity) mContext).downloadSong(song.getUrl(), song.getSong());
            }
        });

        holder.iv_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (song.isFavourite()) {
                    song.setFavourite(false);
                    holder.iv_favourite.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_unfavorite));
                    DatabaseHandler.getInstance(mContext).updateFavouriteSongs(0,song.getUrl());
//                    Utils.addFavourite(mContext, song.getUrl(), false);
                    Toast.makeText(mContext, "Removed from favourites", Toast.LENGTH_SHORT).show();
                } else {


                    song.setFavourite(true);
                    holder.iv_favourite.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_favorite));
                    DatabaseHandler.getInstance(mContext).updateFavouriteSongs(1,song.getUrl());
//                    Utils.addFavourite(mContext, song.getUrl(), true);
                    Toast.makeText(mContext, "Added to favourites", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                shareTextUrl(song.getSong(), song.getUrl());
            }
        });

        new AsyncTask<Object, Object, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Object... params) {

                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }


                try {
                    return Glide.
                            with(mContext).
                            load(song.getCoverImage()).
                            asBitmap().
                            into(1000, 1000).
                            get();
                } catch (final ExecutionException e) {
                    Log.e(TAG, e.getMessage());
                } catch (final InterruptedException e) {
                    Log.e(TAG, e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap theBitmap) {
                if (null != theBitmap) {
                    // The full bitmap should be available here


                    holder.iv_cover.setImageBitmap(Constants.blurRenderScript(theBitmap, 25, mContext));

                }
                ;
            }
        }.execute();

    }

    private boolean isFav(Context context, String url) {


        SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        List<String> recentSongList = new Gson().fromJson(sharedpreferences.getString(Constants.FAVOURITES, ""), new TypeToken<List<String>>() {
        }.getType());


        if (recentSongList != null && songList != null) {


            for (String song : recentSongList) {
                if (song.equals(url)) {
                    return true;

                }

            }
        }


        return false;
    }

    private void shareTextUrl(String title, String message) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, title);
        share.putExtra(Intent.EXTRA_TEXT, message);

        mContext.startActivity(Intent.createChooser(share, "Share link!"));
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }
}