package com.visitome.olaplaystudios.Adapters;

/**
 * Created by nikhilramavath on 17/12/17.
 */

import android.content.Context;

import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.bumptech.glide.Glide;

import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;


import java.util.List;


public class CarouselAdapter extends RecyclerView.Adapter<CarouselAdapter.MyViewHolder> {

    private List<Song> songList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView iv_cover;


        public MyViewHolder(View view) {
            super(view);

            iv_cover = view.findViewById(R.id.iv_cover);

        }
    }


    public CarouselAdapter(List<Song> songList, Context context) {
        this.songList = songList;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_song_coursel, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Song song = songList.get(position);


        holder.iv_cover.setImageBitmap(null);
        Glide.with(mContext).load(song.getCoverImage()).into(holder.iv_cover);

    }

    @Override
    public int getItemCount() {
        return songList.size();
    }
}