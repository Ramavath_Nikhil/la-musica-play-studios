package com.visitome.olaplaystudios.Adapters;

/**
 * Created by nikhilramavath on 17/12/17.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.visitome.olaplaystudios.Activites.MainActivity;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Services.MusicForegroundService;
import com.visitome.olaplaystudios.Utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;

public class BottomsheetSongsListAdapter extends RecyclerView.Adapter<BottomsheetSongsListAdapter.MyViewHolder> {

    private List<Song> songList;
    private Context mContext;
    private boolean isSearch;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView iv_cover;
        public TextView tv_title, tv_artist;
        public RelativeLayout mainlayout;

        public MyViewHolder(View view) {
            super(view);

            iv_cover = view.findViewById(R.id.iv_cover);
            tv_title = view.findViewById(R.id.tv_title);
            tv_artist = view.findViewById(R.id.tv_artist);
            mainlayout = view.findViewById(R.id.mainlayout);
        }
    }


    public BottomsheetSongsListAdapter(List<Song> songList, Context context, boolean isSearch) {
        this.songList = songList;
        this.mContext = context;
        this.isSearch = isSearch;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_bottomsheet_songs, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Song song = songList.get(position);


        holder.iv_cover.setImageBitmap(null);
        holder.tv_title.setText(song.getSong());
        holder.tv_artist.setText(song.getArtists());
        Glide.with(mContext).load(song.getCoverImage()).into(holder.iv_cover);
        holder.mainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isSearch) {
                    ArrayList<Song> temp = new ArrayList<>();
                    temp.add(song);
                    MusicForegroundService.setCurrentPlayList(temp);
                    ((MainActivity) mContext).playSongWithPosition(0);

                } else
                    ((MainActivity) mContext).playSongFromAdapter(position);

            }
        });


    }

    @Override
    public int getItemCount() {
        return songList.size();
    }
}