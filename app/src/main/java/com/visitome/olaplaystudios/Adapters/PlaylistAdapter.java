package com.visitome.olaplaystudios.Adapters;

/**
 * Created by nikhilramavath on 17/12/17.
 */

import android.content.Context;

import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.visitome.olaplaystudios.Listeners.SongAddedToPlaylistListener;
import com.visitome.olaplaystudios.Models.PlayList;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Sqlite.DatabaseHandler;

import java.util.List;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.MyViewHolder> {

    private List<PlayList> playLists;
    private Context mContext;
    private boolean isAddPlaylist;
    private String songUrl;
    private SongAddedToPlaylistListener songAddedToPlaylistListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_playlistName;

        public MyViewHolder(View view) {
            super(view);

            tv_playlistName = view.findViewById(R.id.tv_playlistName);

        }
    }


    public PlaylistAdapter(List<PlayList> playLists, Context context, boolean isAddPlaylist, String songUrl, SongAddedToPlaylistListener songAddedToPlaylistListener) {
        this.playLists = playLists;
        this.isAddPlaylist = isAddPlaylist;
        this.mContext = context;
        this.songUrl = songUrl;
        this.songAddedToPlaylistListener = songAddedToPlaylistListener;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_playlist, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final PlayList playList = playLists.get(position);

        holder.tv_playlistName.setText(playList.getName());

        holder.tv_playlistName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isAddPlaylist) {
                    songAddedToPlaylistListener.onSuccess(true,playList.getName());
                } else {

                    if (DatabaseHandler.getInstance(mContext).addSongToPlaylist(songUrl, playList.getName())) {
                        songAddedToPlaylistListener.onSuccess(true,null);
                    } else {
                        songAddedToPlaylistListener.onSuccess(false,null);
                    }

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return playLists.size();
    }
}