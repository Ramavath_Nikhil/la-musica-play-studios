package com.visitome.olaplaystudios.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.visitome.olaplaystudios.Adapters.SongsListAdapter;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Sqlite.DatabaseHandler;
import com.visitome.olaplaystudios.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikhilramavath on 17/12/17.
 */

public class RecentPlayedFragments extends Fragment {

    private RecyclerView rv_songs;
    private List<Song> songList = new ArrayList<>();
    private SongsListAdapter mAdapter;
    private int currentPage = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_songs_list, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        rv_songs = view.findViewById(R.id.rv_songs);
        songList = Utils.getRecentSongs(getActivity());
        mAdapter = new SongsListAdapter(songList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_songs.setLayoutManager(mLayoutManager);
        rv_songs.setItemAnimator(new DefaultItemAnimator());
        rv_songs.setAdapter(mAdapter);

        rv_songs.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!rv_songs.canScrollVertically(0)) {
                    Toast.makeText(getActivity(), "Fetching more records", Toast.LENGTH_SHORT).show();
                    currentPage = currentPage + 1;
                    songList.addAll(DatabaseHandler.getInstance(getActivity()).getAllSongs(currentPage));
                    mAdapter.notifyDataSetChanged();

                }
            }
        });


    }

    public void notifyAdapter() {
        mAdapter.notifyDataSetChanged();
    }
}
