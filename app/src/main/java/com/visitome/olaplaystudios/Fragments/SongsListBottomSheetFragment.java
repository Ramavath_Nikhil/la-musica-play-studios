package com.visitome.olaplaystudios.Fragments;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;

import com.visitome.olaplaystudios.Activites.MainActivity;
import com.visitome.olaplaystudios.Adapters.BottomsheetSongsListAdapter;
import com.visitome.olaplaystudios.Adapters.SongsListAdapter;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Services.MusicForegroundService;
import com.visitome.olaplaystudios.Sqlite.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikhilramavath on 17/12/17.
 */

public class SongsListBottomSheetFragment extends BottomSheetDialogFragment {

    private int bottomSheetState = -1;
    private BottomSheetBehavior bottomSheetBehaviour;
    private  View contentView;

    private RecyclerView rv_songs;
    private List<Song> songList = new ArrayList<>();
    private BottomsheetSongsListAdapter mAdapter;

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
          contentView = View.inflate(getContext(), R.layout.fragment_bottomsheet_songlist, null);
        dialog.setContentView(contentView);
        init();
    }


    private void init() {

        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();


        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            bottomSheetBehaviour = (BottomSheetBehavior) behavior;
            bottomSheetBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {


                    if (newState == 5)
                        getDialog().cancel();

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });


            contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    contentView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    int height = contentView.getMeasuredHeight();
                    bottomSheetBehaviour.setPeekHeight(height);


                }
            });
        }


        rv_songs = contentView.findViewById(R.id.rv_songs);
        songList = MusicForegroundService.getCurrentPlayList();
        mAdapter = new BottomsheetSongsListAdapter(songList,getActivity(),false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_songs.setLayoutManager(mLayoutManager);
        rv_songs.setItemAnimator(new DefaultItemAnimator());
        rv_songs.setAdapter(mAdapter);
    }
    public boolean isExpanded() {

        if (bottomSheetState == BottomSheetBehavior.STATE_EXPANDED) {
            return true;
        }

        return false;
    }
}
