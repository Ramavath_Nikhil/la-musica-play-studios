package com.visitome.olaplaystudios.Fragments;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;

import com.visitome.olaplaystudios.Adapters.BottomsheetSongsListAdapter;
import com.visitome.olaplaystudios.Adapters.SongsListAdapter;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Sqlite.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikhilramavath on 17/12/17.
 */

public class SearchBottomSheetFragment extends BottomSheetDialogFragment {

    private int bottomSheetState = -1;
    private BottomSheetBehavior bottomSheetBehaviour;
    private  View contentView;

    private RecyclerView rv_search;
    private List<Song> songList = new ArrayList<>();
    private List<Song> searchList = new ArrayList<>();
    private BottomsheetSongsListAdapter mAdapter;

    private EditText ev_search;

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        contentView = View.inflate(getContext(), R.layout.fragment_bottomsheet_search, null);
        dialog.setContentView(contentView);
        init();
    }


    private void init() {

        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();


        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            bottomSheetBehaviour = (BottomSheetBehavior) behavior;
            bottomSheetBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {


                    if (newState == 5)
                        getDialog().cancel();

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });


            contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    contentView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    int height = contentView.getMeasuredHeight();
                    bottomSheetBehaviour.setPeekHeight(height);


                }
            });
        }


        rv_search = contentView.findViewById(R.id.rv_search);
        songList = DatabaseHandler.getInstance(getActivity()).getAllSongs(-1);
        mAdapter = new BottomsheetSongsListAdapter(searchList,getActivity(),true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_search.setLayoutManager(mLayoutManager);
        rv_search.setItemAnimator(new DefaultItemAnimator());
        rv_search.setAdapter(mAdapter);


        ev_search = contentView.findViewById(R.id.ev_search);
        ev_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


                searchList.clear();
                for(Song song: songList)
                {
                    if(song.getSong().toLowerCase().contains(editable.toString().toLowerCase()))
                    {
                        searchList.add(song);
                    }
                }


                mAdapter.notifyDataSetChanged();

            }
        });


    }
    public boolean isExpanded() {

        if (bottomSheetState == BottomSheetBehavior.STATE_EXPANDED) {
            return true;
        }

        return false;
    }
}
