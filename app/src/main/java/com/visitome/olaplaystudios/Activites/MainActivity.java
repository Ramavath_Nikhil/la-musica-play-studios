package com.visitome.olaplaystudios.Activites;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.azoft.carousellayoutmanager.DefaultChildSelectionListener;
import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.gson.Gson;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.visitome.olaplaystudios.Adapters.CarouselAdapter;
import com.visitome.olaplaystudios.Adapters.PlaylistAdapter;
import com.visitome.olaplaystudios.Fragments.SearchBottomSheetFragment;
import com.visitome.olaplaystudios.Fragments.SongsListBottomSheetFragment;
import com.visitome.olaplaystudios.Fragments.SongsListFragment;
import com.visitome.olaplaystudios.Listeners.SongAddedToPlaylistListener;
import com.visitome.olaplaystudios.Models.PlayList;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Services.DownloadServiceTask;
import com.visitome.olaplaystudios.Services.MusicForegroundService;
import com.visitome.olaplaystudios.Sqlite.DatabaseHandler;
import com.visitome.olaplaystudios.Utils.Constants;
import com.visitome.olaplaystudios.Utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SongAddedToPlaylistListener {


    private ImageView iv_menu, iv_search, iv_bottom_thumbnail;
    private View previousArcSeekbar = null;
    private SongsListBottomSheetFragment songsListBottomSheetFragment = null;


    private boolean isPlayerPlaying = false;

    private ImageView iv_bottomCover, iv_bottomPlayPause;
    private TextView tv_bottomTitle, tv_bottomArtist;
    private ImageView iv_slider_bottomCover;
    private TextView tv_slider_bottomTitle, tv_slider_bottomArtist;
    private ImageView iv_song_pause_play, iv_song_prev, iv_song_next, iv_repeat, iv_shuffle, iv_addPlaylist;
    private TextView tv_song_title, tv_song_artist;

    private SongsListFragment songsListFragment;

    private RecyclerView rv_carousel;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_READ_STORAGE = 123;
    private MyBroadcastReceiver_Update songProgressUpdateBrodcast;

    private String dowloadUrl, downloadSongName;

    private Dialog addPlayListDialog;

    private boolean isPrevSong = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        onClickListeners();
        songsListFragment = new SongsListFragment();
        replaceFragment(songsListFragment);

        registerSongProgressReceiver();
        registerDowloadReceiver();
    }

    private void registerSongProgressReceiver() {
        songProgressUpdateBrodcast = new MyBroadcastReceiver_Update();
        IntentFilter intentFilter_update = new IntentFilter(MusicForegroundService.ACTION_MyUpdate);
        intentFilter_update.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(songProgressUpdateBrodcast, intentFilter_update);
    }


    private void registerDowloadReceiver() {
        songProgressUpdateBrodcast = new MyBroadcastReceiver_Update();
        IntentFilter intentFilter_update = new IntentFilter(DownloadServiceTask.ACTION_DOWNLOADUpdate);
        intentFilter_update.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(songProgressUpdateBrodcast, intentFilter_update);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Please provide us permission to store downloaded song in your device");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_READ_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_READ_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {


        finish();
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_READ_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadSong(dowloadUrl, downloadSongName);
                } else {
                    //code for deny
                }
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //un-register BroadcastReceiver

        try {

            unregisterReceiver(songProgressUpdateBrodcast);

            Intent service = new Intent(MainActivity.this, MusicForegroundService.class);
            service.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
            startService(service);

            unregisterReceiver(songProgressUpdateBrodcast);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccess(boolean isAdded, String playPlaylist) {


        if (playPlaylist != null) {
            if (isAdded) {
                ArrayList<Song> playlistSongs = DatabaseHandler.getInstance(MainActivity.this).getAllPlaylistSongs(playPlaylist);
                songsListFragment.setData(playlistSongs, false);
                if (addPlayListDialog != null)
                    addPlayListDialog.cancel();
            }

        } else {
            if (isAdded) {
                Toast.makeText(MainActivity.this, R.string.song_added_to_playlist_, Toast.LENGTH_SHORT).show();
                if (addPlayListDialog != null)
                    addPlayListDialog.cancel();
            } else {
                Toast.makeText(MainActivity.this, R.string.song_already_exists_in_playlist, Toast.LENGTH_SHORT).show();
            }
        }
    }


    public class MyBroadcastReceiver_Update extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                if (intent.hasExtra(MusicForegroundService.EXTRA_KEY_UPDATE)) {
                    int update = intent.getIntExtra(MusicForegroundService.EXTRA_KEY_UPDATE, 0);
                    String songLength = intent.getStringExtra(MusicForegroundService.EXTRA_SONG_LENGTH);
                    String currentPosition = intent.getStringExtra(MusicForegroundService.EXTRA_SONG_CURRENT_TIME);
                    Log.d("song length", songLength + " " + currentPosition);
                    if (previousArcSeekbar != null) {
                        ((ArcProgress) previousArcSeekbar.findViewById(R.id.arc_progress)).setProgress(update);
                        ((TextView) previousArcSeekbar.findViewById(R.id.tv_currentTime)).setText(currentPosition);

                        if (songLength.length() <= 5)
                            ((TextView) previousArcSeekbar.findViewById(R.id.tv_totalTime)).setText(songLength);
                        else
                            ((TextView) previousArcSeekbar.findViewById(R.id.tv_totalTime)).setText("00:00");

                    }
                    if (update >= 100) {

                        Utils.currentPlayingSongURL = MusicForegroundService.getCurrentPlayingSongUrl();
                        updateUIElementsWhilePlaying();
                    }
                } else if (intent.hasExtra(Constants.ACTION.NEXT_ACTION)) {
                    int currentPlayingIndex = intent.getIntExtra(Constants.ACTION.SONG_INDEX, 0);
                    updateUIElementsWhilePlaying();
                } else {
                    Toast.makeText(MainActivity.this, "Download Completed", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void init() {
        iv_menu = findViewById(R.id.iv_menu);
        iv_search = findViewById(R.id.iv_search);


        //Navigation drawer intilization
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        bottomSlideMenu();


    }


    public void addSongs(ArrayList<Song> songs) {
        MusicForegroundService.addMoreSongsToPlaylist(songs);
        rv_carousel.getAdapter().notifyDataSetChanged();
        songsListFragment.notifyAdapter();
    }

    public void playSongFromAdapter(int position) {

        Intent service = new Intent(MainActivity.this, MusicForegroundService.class);
        if (!MusicForegroundService.IS_SERVICE_RUNNING) {
            service.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
            service.putExtra(Constants.ACTION.SONG_INDEX, position);
            MusicForegroundService.IS_SERVICE_RUNNING = true;
        } else {
            service.setAction(Constants.ACTION.CHANGE_SONG_ACTION);
            service.putExtra(Constants.ACTION.SONG_INDEX, position);

        }

        isPlayerPlaying = true;

        MusicForegroundService.setCurrentPlayingPosition(position);
        Utils.currentPlayingSongURL = MusicForegroundService.getCurrentPlayingSongUrl();
        updateUIElementsWhilePlaying();
        rv_carousel.getAdapter().notifyDataSetChanged();
        startService(service);

        ((SlidingUpPanelLayout) findViewById(R.id.sliding_layout)).setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

    }


    public void playSongWithPosition(int position) {

        Log.d("position",position+"");

        Intent service = new Intent(MainActivity.this, MusicForegroundService.class);
        if (!MusicForegroundService.IS_SERVICE_RUNNING) {
            service.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
            service.putExtra(Constants.ACTION.SONG_INDEX, position);
            MusicForegroundService.IS_SERVICE_RUNNING = true;
        } else {
            service.setAction(Constants.ACTION.CHANGE_SONG_ACTION);
            service.putExtra(Constants.ACTION.SONG_INDEX, position);

        }

        isPlayerPlaying = true;
        MusicForegroundService.setCurrentPlayingPosition(position);
        Utils.currentPlayingSongURL = MusicForegroundService.getCurrentPlayingSongUrl();
        updateUIElementsWhilePlaying();
        startService(service);


    }


    public boolean isPlayerPlaying() {
        return isPlayerPlaying;
    }

    public void pausePlayer() {

        Intent service = new Intent(MainActivity.this, MusicForegroundService.class);
        service.setAction(Constants.ACTION.PLAY_PAUSE_ACTION);
        MusicForegroundService.IS_SERVICE_RUNNING = true;
        startService(service);


        if (isPlayerPlaying) {
            isPlayerPlaying = false;
            iv_bottomPlayPause.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
            iv_song_pause_play.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
        } else {
            isPlayerPlaying = true;
            iv_bottomPlayPause.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
            iv_song_pause_play.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
        }


        updateUIElementsWhilePlaying();


    }

    public void playNextSong() {


        Intent service = new Intent(MainActivity.this, MusicForegroundService.class);
        service.setAction(Constants.ACTION.NEXT_ACTION);
        MusicForegroundService.IS_SERVICE_RUNNING = true;
        startService(service);


    }


    public void playPrevSong() {

        isPrevSong = true;
        Intent service = new Intent(MainActivity.this, MusicForegroundService.class);
        service.setAction(Constants.ACTION.PREV_ACTION);
        MusicForegroundService.IS_SERVICE_RUNNING = true;
        startService(service);


    }


    private void updateUIElementsWhilePlaying() {

        int currentPlayingSongPosition = MusicForegroundService.getCurrentPlayingPosition();
        Song song = MusicForegroundService.getCurrentPlayList().get(currentPlayingSongPosition);


        tv_bottomTitle.setText(song.getSong());
        tv_bottomArtist.setText(song.getArtists());
        tv_bottomTitle.setText(song.getSong());
        Glide.with(MainActivity.this).load(song.getCoverImage()).into(iv_bottom_thumbnail);
        Glide.with(MainActivity.this).load(song.getCoverImage()).into(iv_bottomCover);

        if (isPlayerPlaying) {

            iv_bottomPlayPause.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
            iv_song_pause_play.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
        } else {

            iv_bottomPlayPause.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
            iv_song_pause_play.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
        }

        tv_slider_bottomTitle.setText(song.getSong());
        tv_slider_bottomArtist.setText(song.getArtists());
        Glide.with(MainActivity.this).load(song.getCoverImage()).into(iv_slider_bottomCover);


        tv_song_artist.setText(song.getArtists());
        tv_song_title.setText(song.getSong());

        rv_carousel.smoothScrollToPosition(currentPlayingSongPosition);


        songsListFragment.notifyAdapter();


        iv_song_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                playNextSong();
            }
        });

        iv_song_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                playPrevSong();
            }
        });


        iv_song_pause_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pausePlayer();
            }
        });

        iv_bottomPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pausePlayer();
            }
        });


    }


    public void replaceFragment(Fragment fragment) {
        //replacing the fragment
        if (fragment != null) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.content_frame, fragment);


            ft.addToBackStack(null);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void bottomSlideMenu() {
        final SlidingUpPanelLayout mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            public static final String TAG = "";

            @Override
            public void onPanelSlide(View panel, float slideOffset) {


                findViewById(R.id.rl_topview).setAlpha((1 - slideOffset));
                findViewById(R.id.rlPlayerLayout).setAlpha((slideOffset));


            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.i(TAG, "onPanelStateChanged " + newState);

                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {

                    findViewById(R.id.rl_topview).setVisibility(View.INVISIBLE);
                } else if (newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    findViewById(R.id.rl_topview).setVisibility(View.VISIBLE);
                }

            }
        });
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        tv_bottomTitle = findViewById(R.id.tv_bottom_title);
        tv_bottomArtist = findViewById(R.id.tv_bottom_artist);
        iv_bottomCover = findViewById(R.id.iv_cover);
        iv_bottom_thumbnail = findViewById(R.id.iv_thumbnail);
        iv_bottomPlayPause = findViewById(R.id.iv_bottom_play_pause);
        iv_slider_bottomCover = findViewById(R.id.iv_slider_cover);
        tv_slider_bottomArtist = findViewById(R.id.tv_slider_artist);
        tv_slider_bottomTitle = findViewById(R.id.tv_slider_title);

        tv_song_artist = findViewById(R.id.tv_song_artist);
        tv_song_title = findViewById(R.id.tv_song_title);
        iv_song_pause_play = findViewById(R.id.fb_song_play_pause);
        iv_song_next = findViewById(R.id.iv_song_next);
        iv_song_prev = findViewById(R.id.iv_song_prev);

        rv_carousel = findViewById(R.id.rv_carousel);

        iv_repeat = findViewById(R.id.iv_repeat);
        iv_shuffle = findViewById(R.id.iv_shuffle);
        iv_addPlaylist = findViewById(R.id.iv_addPlaylist);


        if (Utils.getShuffleMode(MainActivity.this)) {
            iv_shuffle.setImageDrawable(getResources().getDrawable(R.drawable.ic_shuffle_red));

        } else {
            iv_shuffle.setImageDrawable(getResources().getDrawable(R.drawable.ic_shuffle_white));
        }


        if (Utils.getRepeatMode(MainActivity.this) == Constants.NO_REPEAT) {

            iv_repeat.setImageDrawable(getResources().getDrawable(R.drawable.ic_repeat_white));
        } else if (Utils.getRepeatMode(MainActivity.this) == Constants.REPEAT_ALL) {

            iv_repeat.setImageDrawable(getResources().getDrawable(R.drawable.ic_repeat_red));
        } else {

            iv_repeat.setImageDrawable(getResources().getDrawable(R.drawable.ic_repeat_one));
        }


        CarouselAdapter mAdapter = new CarouselAdapter(MusicForegroundService.getCurrentPlayList(), this);


        initRecyclerView(rv_carousel, new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, false), mAdapter);
        Glide.with(MainActivity.this).load("http://hck.re/5dh4D5").into(iv_bottom_thumbnail);


        final float[] dX = new float[1];
        final float[] dY = new float[1];

        findViewById(R.id.rl_bottomLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (songsListBottomSheetFragment == null) {
                    songsListBottomSheetFragment = new SongsListBottomSheetFragment();
                    songsListBottomSheetFragment.show(getSupportFragmentManager(), songsListBottomSheetFragment.getTag());
                } else if (!songsListBottomSheetFragment.isExpanded()) {

                    if (!songsListBottomSheetFragment.isAdded())
                        songsListBottomSheetFragment.show(getSupportFragmentManager(), songsListBottomSheetFragment.getTag());

                }
            }
        });
        findViewById(R.id.rl_bottomLayout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:

                        dX[0] = view.getX() - event.getRawX();
                        dY[0] = view.getY() - event.getRawY();
                        break;

                    case MotionEvent.ACTION_MOVE:


                        if (view.getY() - event.getRawY() > dY[0]) {


                            if (songsListBottomSheetFragment == null) {
                                songsListBottomSheetFragment = new SongsListBottomSheetFragment();
                                songsListBottomSheetFragment.show(getSupportFragmentManager(), songsListBottomSheetFragment.getTag());
                            } else if (!songsListBottomSheetFragment.isExpanded()) {

                                if (!songsListBottomSheetFragment.isAdded())
                                    songsListBottomSheetFragment.show(getSupportFragmentManager(), songsListBottomSheetFragment.getTag());

                            }


                        } else {

                        }


                        break;
                    default:
                        return false;
                }
                return true;

            }
        });
    }

    private void onClickListeners() {
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (!drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.openDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.START);
                }

            }
        });


        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        findViewById(R.id.iv_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SearchBottomSheetFragment searchBottomSheetFragment = new SearchBottomSheetFragment();
                searchBottomSheetFragment.show(getSupportFragmentManager(), searchBottomSheetFragment.getTag());
            }
        });

        iv_shuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.getShuffleMode(MainActivity.this)) {
                    iv_shuffle.setImageDrawable(getResources().getDrawable(R.drawable.ic_shuffle_white));

                } else {
                    iv_shuffle.setImageDrawable(getResources().getDrawable(R.drawable.ic_shuffle_red));
                }

                Utils.setShuffleMode(MainActivity.this);
            }
        });

        iv_repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.getRepeatMode(MainActivity.this) == Constants.NO_REPEAT) {
                    Utils.setRepeatMode(MainActivity.this);
                    iv_repeat.setImageDrawable(getResources().getDrawable(R.drawable.ic_repeat_red));
                } else if (Utils.getRepeatMode(MainActivity.this) == Constants.REPEAT_ALL) {
                    Utils.setRepeatMode(MainActivity.this);
                    iv_repeat.setImageDrawable(getResources().getDrawable(R.drawable.ic_repeat_one));
                } else {
                    Utils.setRepeatMode(MainActivity.this);
                    iv_repeat.setImageDrawable(getResources().getDrawable(R.drawable.ic_repeat_white));
                }
            }
        });

        iv_addPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddPlaylistDialog(false);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        displaySelectedScreen(item.getItemId());


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Open downloaded folder
    private void openDownloadedFolder() {
        //First check if SD Card is present or not

        if (checkPermission()) {
            if (Utils.isSDCardPresent()) {

                //Get Download Directory File
                File apkStorage = new File(
                        Environment.getExternalStorageDirectory() + "/"
                                + Constants.downloadDirectory);

                //If file is not present then display Toast
                if (!apkStorage.exists())
                    Toast.makeText(MainActivity.this, "Right now there is no directory. Please download some file first.", Toast.LENGTH_SHORT).show();

                else {

                    //If directory is present Open Folder

                    /** Note: Directory will open only if there is a app to open directory like File Manager, etc.  **/

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Constants.downloadDirectory);
                    intent.setDataAndType(uri, "file/*");
                    startActivity(Intent.createChooser(intent, "Open Download Folder"));
                }

            } else
                Toast.makeText(MainActivity.this, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();
        }


    }

    public void downloadSong(String url, String songName) {
        dowloadUrl = url;

        Log.d("download url", dowloadUrl);
        if (checkPermission()) {
            Log.d("permission checked", dowloadUrl);
            Intent service = new Intent(MainActivity.this, DownloadServiceTask.class);
            if (!DownloadServiceTask.IS_SERVICE_RUNNING) {
                Toast.makeText(this, "Downloading song", Toast.LENGTH_SHORT).show();
                service.setAction(Constants.ACTION.DOWNLOAD_START);
                service.putExtra(Constants.ACTION.SONG_INDEX, url);
                service.putExtra(Constants.ACTION.DOWNLOAD_SONG_NAME, songName);
                Log.d("download started", "");
            } else {
                service.setAction(Constants.ACTION.DOWNLOAD_END);
                Toast.makeText(this, "Download cancelled", Toast.LENGTH_SHORT).show();
                Log.d("download endede", "");
            }
            startService(service);
        } else {
            Log.d("permission issue", "");
        }
    }

    private void displaySelectedScreen(int itemId) {
        ArrayList<Song> songList;
        switch (itemId) {
            case R.id.nav_home:

                songList = DatabaseHandler.getInstance(this).getAllSongs(-1);
//                MusicForegroundService.setCurrentPlayList(songList);
                songsListFragment.setData(songList, false);


                break;

            case R.id.nav_download:
//                openDownloadedFolder();
                songList = (ArrayList<Song>) DatabaseHandler.getInstance(MainActivity.this).getDownloadedSongs();
                songsListFragment.setData(songList, false);
                break;

            case R.id.nav_fav:


                songList = (ArrayList<Song>) DatabaseHandler.getInstance(MainActivity.this).getFavouriteSongs();
//                MusicForegroundService.setCurrentPlayList(songList);
                songsListFragment.setData(songList, false);

                break;


            case R.id.nav_recent:

                songList = (ArrayList<Song>) Utils.getRecentSongs(MainActivity.this);
//                MusicForegroundService.setCurrentPlayList(songList);
                songsListFragment.setData(songList, false);

                break;

            case R.id.nav_playlist:
                showAddPlaylistDialog(true);
                break;

            case R.id.nav_logout:

                finish();
                break;
        }
    }


    private void initRecyclerView(final RecyclerView recyclerView, final CarouselLayoutManager layoutManager, final CarouselAdapter adapter) {
        // enable zoom effect. this line can be customized
        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        layoutManager.setMaxVisibleItems(1);

        recyclerView.setLayoutManager(layoutManager);
        // we expect only fixed sized item for now
        recyclerView.setHasFixedSize(true);
        // sample adapter with random data
        recyclerView.setAdapter(adapter);
        // enable center post scrolling
        recyclerView.addOnScrollListener(new CenterScrollListener());
        recyclerView.scrollToPosition(adapter.getItemCount() / 2);
        // enable center post touching on item and item click listener
        DefaultChildSelectionListener.initCenterItemListener(new DefaultChildSelectionListener.OnCenterItemClickListener() {
            @Override
            public void onCenterItemClicked(@NonNull final RecyclerView recyclerView, @NonNull final CarouselLayoutManager carouselLayoutManager, @NonNull final View v) {
                final int position = recyclerView.getChildLayoutPosition(v);
                final String msg = String.format(Locale.US, "Item %1$d was clicked", position);
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        }, recyclerView, layoutManager);


        layoutManager.addOnItemSelectionListener(new CarouselLayoutManager.OnCenterItemSelectionListener() {

            @Override
            public void onCenterItemChanged(final int adapterPosition) {
                if (CarouselLayoutManager.INVALID_POSITION != adapterPosition) {

                    try {
                        if (previousArcSeekbar != null) {
                            previousArcSeekbar.findViewById(R.id.arc_progress).setVisibility(View.GONE);
                            ((TextView) previousArcSeekbar.findViewById(R.id.tv_currentTime)).setText("");
                            ((TextView) previousArcSeekbar.findViewById(R.id.tv_totalTime)).setText("");

                        }
                        previousArcSeekbar = recyclerView.findViewHolderForAdapterPosition(adapterPosition).itemView;
                        previousArcSeekbar.findViewById(R.id.arc_progress).setVisibility(View.VISIBLE);

                        int currentPlayingSongPosition = MusicForegroundService.getCurrentPlayingPosition();
                        if (currentPlayingSongPosition != -1 && currentPlayingSongPosition != adapterPosition && ((SlidingUpPanelLayout) findViewById(R.id.sliding_layout)).getPanelState() == (SlidingUpPanelLayout.PanelState.EXPANDED)) {
                            if ((currentPlayingSongPosition != 0 && adapterPosition != (MusicForegroundService.getCurrentPlayList().size() - 1)))
                                if (!isPrevSong)
                                    playSongWithPosition(adapterPosition);
                                else
                                    isPrevSong = false;

                        }


                        if (iv_bottom_thumbnail != null && MusicForegroundService.getCurrentPlayList() != null) {
                            Glide.with(MainActivity.this).load(MusicForegroundService.getCurrentPlayList().get(adapterPosition).getCoverImage()).into(iv_bottom_thumbnail);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }


    private void showAddPlaylistDialog(boolean isAdd) {
        addPlayListDialog = new Dialog(MainActivity.this);
        addPlayListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addPlayListDialog.setContentView(R.layout.layout_dialog_playlists);
        addPlayListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final ArrayList<PlayList> playLists = DatabaseHandler.getInstance(MainActivity.this).getAllPlaylists();
        final RecyclerView rv_playlist = addPlayListDialog.findViewById(R.id.rv_playlist);
        PlaylistAdapter mAdapter = new PlaylistAdapter(playLists, MainActivity.this, isAdd, MusicForegroundService.getCurrentPlayingSongUrl(), MainActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
        rv_playlist.setLayoutManager(mLayoutManager);
        rv_playlist.setItemAnimator(new DefaultItemAnimator());
        rv_playlist.setAdapter(mAdapter);

        final LinearLayout ll_playlist_title = addPlayListDialog.findViewById(R.id.ll_playlist_title);
        final LinearLayout ll_playlist_add_playlist = addPlayListDialog.findViewById(R.id.ll_createNewPlaylist);
        TextView tv_createPlaylist = addPlayListDialog.findViewById(R.id.tv_createPlaylist);
        Button bAddPlaylist = addPlayListDialog.findViewById(R.id.bAddPlaylist);
        final EditText ev_creat_playlist = addPlayListDialog.findViewById(R.id.ev_creat_playlist);


        if (playLists != null && playLists.size() > 0) {
            ll_playlist_title.setVisibility(View.VISIBLE);

        } else {
            ll_playlist_title.setVisibility(View.GONE);
        }


        tv_createPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ll_playlist_add_playlist.getVisibility() == View.GONE)
                    ll_playlist_add_playlist.setVisibility(View.VISIBLE);
                else
                    ll_playlist_add_playlist.setVisibility(View.GONE);
            }
        });

        bAddPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ev_creat_playlist.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Please enter some name", Toast.LENGTH_SHORT).show();
                } else {
                    if (DatabaseHandler.getInstance(MainActivity.this).addPlaylist(ev_creat_playlist.getText().toString())) {

                        playLists.clear();
                        playLists.addAll(DatabaseHandler.getInstance(MainActivity.this).getAllPlaylists());
                        if (playLists != null && playLists.size() > 0) {
                            ll_playlist_title.setVisibility(View.VISIBLE);
                            rv_playlist.getAdapter().notifyDataSetChanged();

                        } else {
                            ll_playlist_title.setVisibility(View.GONE);
                        }


                    }
                }

            }
        });

        addPlayListDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(addPlayListDialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels - 50;


        addPlayListDialog.getWindow().setLayout(width, lp.height);
    }


}
