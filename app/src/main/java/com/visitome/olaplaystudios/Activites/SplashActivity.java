package com.visitome.olaplaystudios.Activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.R;
import com.visitome.olaplaystudios.Retrofit.RestClient;
import com.visitome.olaplaystudios.Sqlite.DatabaseHandler;
import com.visitome.olaplaystudios.Utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        fetchSongsList();

    }

    private void fetchSongsList() {

        Call<List<Song>> call = new RestClient(Constants.BASE_URL).get().fetchSongsList();

        call.enqueue(new Callback<List<Song>>() {
            @Override
            public void onResponse(Call<List<Song>> call, Response<List<Song>> response) {

                if (response.body() != null && response.body().size() > 0) {



                    DatabaseHandler.getInstance(SplashActivity.this).addSongs(response.body());
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);

                    // close this activity
                    finish();
                } else {
                    Toast.makeText(SplashActivity.this, "Something went wrong,Please check your internet connection", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                }

            }

            @Override
            public void onFailure(Call<List<Song>> call, Throwable t) {

                Toast.makeText(SplashActivity.this, "Something went wrong,Please check your internet connection", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                t.printStackTrace();
            }
        });
    }
}
