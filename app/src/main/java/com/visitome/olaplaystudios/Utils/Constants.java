package com.visitome.olaplaystudios.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

/**
 * Created by nikhilramavath on 17/12/17.
 */

public class Constants {


    public static final String BASE_URL = "http://starlord.hackerearth.com/";
    public static final String recentSongs = "recentSongs";
    public static final String FAVOURITES = "fav";
    public static final String REPEAT_MODE = "repeat_mode";
    public static final String SHUFFLE_MODE = "shuffle_mode";


    public static final int NO_REPEAT = 0;
    public static final int REPEAT_ALL = 1;
    public static final int REPEAT_ONCE = 2;


    public static final String downloadDirectory = "Ola play studios";
    public interface ACTION {
        public static String MAIN_ACTION = "com.visitome.olaplaystudios.Services.action.main";
        public static String INIT_ACTION = "com.visitome.olaplaystudios.Services.action.init";
        public static String PREV_ACTION = "com.visitome.olaplaystudios.Services.action.prev";
        public static String PLAY_PAUSE_ACTION = "com.visitome.olaplaystudios.Services.action.play";
        public static String NEXT_ACTION = "com.visitome.olaplaystudios.Services.action.next";
        public static String STARTFOREGROUND_ACTION = "com.visitome.olaplaystudios.Services.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.visitome.olaplaystudios.Services.action.stopforeground";
        public static String SONG_INDEX = "com.visitome.olaplaystudios.Services.action.songindex";
        public static String CHANGE_SONG_ACTION = "com.visitome.olaplaystudios.Services.action.changesongaction";
        public static String UPDATE_SONG_LIST = "com.visitome.olaplaystudios.Services.action.updatesonglist";
        public static String DOWNLOAD_START = "com.visitome.olaplaystudios.Services.action.downloadstart";
        public static String DOWNLOAD_END = "com.visitome.olaplaystudios.Services.action.downloadstart";
        public static String DOWNLOAD_SONG_NAME = "com.visitome.olaplaystudios.Services.action.downloadsongname";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }




    @SuppressLint("NewApi")
    public static Bitmap blurRenderScript(Bitmap smallBitmap, int radius, Context context) {

        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    private static  Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }
}
