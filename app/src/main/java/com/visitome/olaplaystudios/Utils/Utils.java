package com.visitome.olaplaystudios.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.visitome.olaplaystudios.Models.Song;
import com.visitome.olaplaystudios.Sqlite.DatabaseHandler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by nikhilramavath on 17/12/17.
 */

public class Utils {

    public static String currentPlayingSongURL = "";
    public static final String MyPREFERENCES = "MyPrefs";
    private static SharedPreferences sharedpreferences;




    public static void addSongToRecents(Context context, String songUrl) {


        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        List<String> songList = new Gson().fromJson(sharedpreferences.getString(Constants.recentSongs, ""), new TypeToken<List<String>>() {
        }.getType());

        if (songList == null)
            songList = new ArrayList<>();

        Iterator<String> iterator = songList.iterator();
        while (iterator.hasNext()) {
            String value = iterator.next();
            if (songUrl.equals(value)) {
                iterator.remove();
                break;
            }
        }
        songList.add(0, songUrl);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.recentSongs, new Gson().toJson(songList));
        editor.apply();
        Log.d("Recent items", sharedpreferences.getString(Constants.recentSongs, ""));

    }


//    public static void addFavourite(Context context, String songUrl, boolean isFav) {
//
//
//        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//
//        List<String> favourites = new Gson().fromJson(sharedpreferences.getString(Constants.FAVOURITES, ""), new TypeToken<List<String>>() {
//        }.getType());
//
//        if (favourites == null)
//            favourites = new ArrayList<>();
//
//        Iterator<String> iterator = favourites.iterator();
//        while (iterator.hasNext()) {
//            String value = iterator.next();
//            if (songUrl.equals(value)) {
//                iterator.remove();
//                break;
//            }
//        }
//
//        if (isFav)
//            favourites.add(songUrl);
//
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString(Constants.FAVOURITES, new Gson().toJson(favourites));
//        editor.apply();
//
//
//    }

    public static List<Song> getRecentSongs(Context context) {


        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        List<Song> songList = DatabaseHandler.getInstance(context).getAllSongs(-1);
        List<String> recentSongList = new Gson().fromJson(sharedpreferences.getString(Constants.recentSongs, ""), new TypeToken<List<String>>() {
        }.getType());
        List<Song> recenetSongsList = new ArrayList<>();
        if (recentSongList != null && songList != null) {

            for (Song song : songList) {
                for (String url : recentSongList) {
                    if (song.getUrl().equals(url)) {
                        recenetSongsList.add(song);

                    }
                }
            }
        }

        Log.d("recenet size", recenetSongsList.size() + "");

        return recenetSongsList;
    }

    public static boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(

                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

//    public static List<Song> getFavourites(Context context) {
//
//
//        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//        List<Song> songList = DatabaseHandler.getInstance(context).getAllSongs(-1);
//        List<String> recentSongList = new Gson().fromJson(sharedpreferences.getString(Constants.FAVOURITES, ""), new TypeToken<List<String>>() {
//        }.getType());
//        List<Song> recenetSongsList = new ArrayList<>();
//        if (recentSongList != null && songList != null) {
//
//            for (Song song : songList) {
//                for (String url : recentSongList) {
//                    if (song.getUrl().equals(url)) {
//                        recenetSongsList.add(song);
//                        song.setFavourite(true);
//
//                    }
//                }
//            }
//        }
//
//        Log.d("favoourite size", recenetSongsList.size() + "");
//        return recenetSongsList;
//    }

    public static void setShuffleMode(Context context) {
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(Constants.SHUFFLE_MODE, !getShuffleMode(context));
        editor.apply();


    }

    public static boolean getShuffleMode(Context context) {
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        return sharedpreferences.getBoolean(Constants.SHUFFLE_MODE, false);

    }


    public static void setRepeatMode(Context context)
    {
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if(getRepeatMode(context)  == Constants.NO_REPEAT)
        {
            editor.putInt(Constants.REPEAT_MODE, Constants.REPEAT_ALL);
            editor.apply();
        }
        else  if(getRepeatMode(context)  == Constants.REPEAT_ALL)
        {
            editor.putInt(Constants.REPEAT_MODE, Constants.REPEAT_ONCE);
            editor.apply();
        }
        else
        {
            editor.putInt(Constants.REPEAT_MODE, Constants.NO_REPEAT);
            editor.apply();
        }

    }

    public static int getRepeatMode(Context context) {
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        return sharedpreferences.getInt(Constants.REPEAT_MODE,Constants.NO_REPEAT);

    }
}