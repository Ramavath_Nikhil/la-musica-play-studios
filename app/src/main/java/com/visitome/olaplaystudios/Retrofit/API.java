package com.visitome.olaplaystudios.Retrofit;


import com.visitome.olaplaystudios.Models.Song;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface API {

    @GET("studio")
    Call<List<Song>> fetchSongsList();


}
